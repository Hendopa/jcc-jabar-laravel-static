
<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/home', function () {
//     return view('home');
// });

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/welcome', function () {
//     return view('welcome');
// });

// Route::get('/', 'HomeController@home');

// Route::get('/register', 'AuthController@register');

// Route::get('/welcome', 'AuthController@welcome');
// Route::post('/welcome', 'AuthController@welcome_post');
Route::get('/', 'CastController@index');
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast/create', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

// Route::get('/data', function () {
//     return view('admin.items.index');
// });
// Route::get('/data-tables', function () {
//     return view('admin.items.data');
// });