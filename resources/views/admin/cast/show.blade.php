@extends('admin.adminlte.master')

@section('content')

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tampikan Data</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      <div>
      <h2>Show Cast {{$cast->id}}</h2>
<h4>Nama :{{$cast->nama}}</h4>
<h4>Umur :{{$cast->umur}}</h4>
<p>Bio :{{$cast->bio}}</p>
</div>
</div>
</div>

@endsection