@extends('admin.adminlte.master')

@section('content')

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tambahkan Data</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      <div>
    <h2>Tambah Data</h2>
        <form action="/cast/create" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <textarea id="bio" name="bio" rows="4" cols="50"></textarea>
            </div>
            <button type="SUBMIT" class="btn btn-primary">Tambah</button>
        </form>
</div>
</div>
</div>

@endsection